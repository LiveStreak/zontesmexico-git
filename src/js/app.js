/* Esta es la Seccion del cmodal de Colores */
// guardamos el id de la imagen visible
idImagenVisible = "contenido1";

function mostrar(id) {
    // esconedemos la imagen visible
    document.getElementById(idImagenVisible).style.display = "none";
    // mostramos la imagen seleccionada
    document.getElementById(id).style.display = "block";
    // guardamos en la variable la imagen que estamos visualizando
    idImagenVisible = id;
}

// Cargamos el modal de colores
if (document.getElementById("boton-color")) {
    var modal = document.getElementById("modal-color");
    var btn = document.getElementById("boton-color");
    var span = document.getElementsByClassName("boton-cerrar")[0];
    var body = document.getElementsByTagName("body")[0];

    btn.onclick = function() {
        modal.style.display = "block";

        body.style.position = "static";
        body.style.height = "100%";
        body.style.overflow = "hidden";
    }

    span.onclick = function() {
        modal.style.display = "none";

        body.style.position = "inherit";
        body.style.height = "auto";
        body.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";

            body.style.position = "inherit";
            body.style.height = "auto";
            body.style.overflow = "visible";
        }
    }
}
// Efecto slider


// Creando botin de prueba de Manejo

if (document.getElementById("boton-prueba")) {
    var modal2 = document.getElementById("modal-pruebaManejo");
    var btn_2 = document.getElementById("boton-prueba");
    var span_2 = document.getElementsByClassName("boton-close")[0];
    var body_2 = document.getElementsByTagName("body")[0];

    btn_2.onclick = function() {
        modal2.style.display = "block";

        body_2.style.position = "static";
        body_2.style.height = "100%";
        body_2.style.overflow = "hidden";
    }

    span_2.onclick = function() {
        modal2.style.display = "none";

        body_2.style.position = "inherit";
        body_2.style.height = "auto";
        body_2.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal2) {
            modal2.style.display = "none";

            body_2.style.position = "inherit";
            body_2.style.height = "auto";
            body_2.style.overflow = "visible";
        }
    }
}
//
if (document.getElementById("boton-cotizar")) {
    var modal3 = document.getElementById("modal-cotizar");
    var btn_3 = document.getElementById("boton-cotizar");
    var span_3 = document.getElementsByClassName("boton-off")[0];
    var body_3 = document.getElementsByTagName("body")[0];

    btn_3.onclick = function() {
        modal3.style.display = "block";

        body_3.style.position = "static";
        body_3.style.height = "100%";
        body_3.style.overflow = "hidden";
    }

    span_3.onclick = function() {
        modal3.style.display = "none";

        body_3.style.position = "inherit";
        body_3.style.height = "auto";
        body_3.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal3) {
            modal3.style.display = "none";

            body_3.style.position = "inherit";
            body_3.style.height = "auto";
            body_3.style.overflow = "visible";
        }
    }
}
// boton de efecto 360 grados
if (document.getElementById("boton-360")) {
    var modal4 = document.getElementById("modal-efecto");
    var btn_4 = document.getElementById("boton-360");
    var span_4 = document.getElementsByClassName("boton-crl")[0];
    var body_4 = document.getElementsByTagName("body")[0];

    btn_4.onclick = function() {
        modal4.style.display = "block";

        body_4.style.position = "static";
        body_4.style.height = "100%";
        body_4.style.overflow = "hidden";
    }

    span_4.onclick = function() {
        modal4.style.display = "none";

        body_4.style.position = "inherit";
        body_4.style.height = "auto";
        body_4.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal4) {
            modal4.style.display = "none";

            body_4.style.position = "inherit";
            body_4.style.height = "auto";
            body_4.style.overflow = "visible";
        }
    }
}


// Hotspots 
//// Hotspot 1
if (document.getElementById("hotspot_1")) {
    var modal5 = document.getElementById("contenido_hotspot1");
    var btn_5 = document.getElementById("hotspot_1");
    var span_5 = document.getElementsByClassName("hot1-close")[0];
    var body_5 = document.getElementsByTagName("body")[0];

    btn_5.onclick = function() {
        modal5.style.display = "block";

        body_5.style.position = "static";
        body_5.style.height = "100%";
        body_5.style.overflow = "hidden";
    }

    span_5.onclick = function() {
        modal5.style.display = "none";

        body_5.style.position = "inherit";
        body_5.style.height = "auto";
        body_5.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal5) {
            modal5.style.display = "none";

            body_5.style.position = "inherit";
            body_5.style.height = "auto";
            body_5.style.overflow = "visible";
        }
    }
}
//// Hotspot 2
if (document.getElementById("hotspot_2")) {
    var modal6 = document.getElementById("contenido_hotspot2");
    var btn_6 = document.getElementById("hotspot_2");
    var span_6 = document.getElementsByClassName("hot2-close")[0];
    var body_6 = document.getElementsByTagName("body")[0];

    btn_6.onclick = function() {
        modal6.style.display = "block";

        body_6.style.position = "static";
        body_6.style.height = "100%";
        body_6.style.overflow = "hidden";
    }

    span_6.onclick = function() {
        modal6.style.display = "none";

        body_6.style.position = "inherit";
        body_6.style.height = "auto";
        body_6.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal6) {
            modal6.style.display = "none";

            body_6.style.position = "inherit";
            body_6.style.height = "auto";
            body_6.style.overflow = "visible";
        }
    }
}
//// Hotspot 3
if (document.getElementById("hotspot_3")) {
    var modal7 = document.getElementById("contenido_hotspot3");
    var btn_7 = document.getElementById("hotspot_3");
    var span_7 = document.getElementsByClassName("hot3-close")[0];
    var body_7 = document.getElementsByTagName("body")[0];

    btn_7.onclick = function() {
        modal7.style.display = "block";

        body_7.style.position = "static";
        body_7.style.height = "100%";
        body_7.style.overflow = "hidden";
    }

    span_7.onclick = function() {
        modal7.style.display = "none";

        body_7.style.position = "inherit";
        body_7.style.height = "auto";
        body_7.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal7) {
            modal7.style.display = "none";

            body_7.style.position = "inherit";
            body_7.style.height = "auto";
            body_7.style.overflow = "visible";
        }
    }
}
//// Hotspot 4
if (document.getElementById("hotspot_4")) {
    var modal8 = document.getElementById("contenido_hotspot4");
    var btn_8 = document.getElementById("hotspot_4");
    var span_8 = document.getElementsByClassName("hot4-close")[0];
    var body_8 = document.getElementsByTagName("body")[0];

    btn_8.onclick = function() {
        modal8.style.display = "block";

        body_8.style.position = "static";
        body_8.style.height = "100%";
        body_8.style.overflow = "hidden";
    }

    span_8.onclick = function() {
        modal8.style.display = "none";

        body_8.style.position = "inherit";
        body_8.style.height = "auto";
        body_8.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal8) {
            modal8.style.display = "none";

            body_8.style.position = "inherit";
            body_8.style.height = "auto";
            body_8.style.overflow = "visible";
        }
    }
}
//// Hotspot 5
if (document.getElementById("hotspot_5")) {
    var modal9 = document.getElementById("contenido_hotspot5");
    var btn_9 = document.getElementById("hotspot_5");
    var span_9 = document.getElementsByClassName("hot5-close")[0];
    var body_9 = document.getElementsByTagName("body")[0];

    btn_9.onclick = function() {
        modal9.style.display = "block";

        body_9.style.position = "static";
        body_9.style.height = "100%";
        body_9.style.overflow = "hidden";
    }

    span_9.onclick = function() {
        modal9.style.display = "none";

        body_9.style.position = "inherit";
        body_9.style.height = "auto";
        body_9.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal9) {
            modal9.style.display = "none";

            body_9.style.position = "inherit";
            body_9.style.height = "auto";
            body_9.style.overflow = "visible";
        }
    }
}
//// Hotspot 6
if (document.getElementById("hotspot_6")) {
    var modal10 = document.getElementById("contenido_hotspot6");
    var btn_10 = document.getElementById("hotspot_6");
    var span_10 = document.getElementsByClassName("hot6-close")[0];
    var body_10 = document.getElementsByTagName("body")[0];

    btn_10.onclick = function() {
        modal10.style.display = "block";

        body_10.style.position = "static";
        body_10.style.height = "100%";
        body_10.style.overflow = "hidden";
    }

    span_10.onclick = function() {
        modal10.style.display = "none";

        body_10.style.position = "inherit";
        body_10.style.height = "auto";
        body_10.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal10) {
            modal10.style.display = "none";

            body_10.style.position = "inherit";
            body_10.style.height = "auto";
            body_10.style.overflow = "visible";
        }
    }
}
//// Hotspot 7
if (document.getElementById("hotspot_7")) {
    var modal11 = document.getElementById("contenido_hotspot7");
    var btn_11 = document.getElementById("hotspot_7");
    var span_11 = document.getElementsByClassName("hot7-close")[0];
    var body_11 = document.getElementsByTagName("body")[0];

    btn_11.onclick = function() {
        modal11.style.display = "block";

        body_11.style.position = "static";
        body_11.style.height = "100%";
        body_11.style.overflow = "hidden";
    }

    span_11.onclick = function() {
        modal11.style.display = "none";

        body_11.style.position = "inherit";
        body_11.style.height = "auto";
        body_11.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal11) {
            modal11.style.display = "none";

            body_11.style.position = "inherit";
            body_11.style.height = "auto";
            body_11.style.overflow = "visible";
        }
    }
}
if (document.getElementById("hotspot_8")) {
    var modal12 = document.getElementById("contenido_hotspot8");
    var btn_12 = document.getElementById("hotspot_8");
    var span_12 = document.getElementsByClassName("hot8-close")[0];
    var body_12 = document.getElementsByTagName("body")[0];

    btn_12.onclick = function() {
        modal12.style.display = "block";

        body_12.style.position = "static";
        body_12.style.height = "100%";
        body_12.style.overflow = "hidden";
    }

    span_12.onclick = function() {
        modal12.style.display = "none";

        body_12.style.position = "inherit";
        body_12.style.height = "auto";
        body_12.style.overflow = "visible";
    }

    window.onclick = function(event) {
        if (event.target == modal12) {
            modal12.style.display = "none";

            body_12.style.position = "inherit";
            body_12.style.height = "auto";
            body_12.style.overflow = "visible";
        }
    }
}